import React, {useEffect, useState} from "react";
import Header from "./components/Header";
import Cat from "./components/Cat";
//import './App.css';

function App() {
  const [character, setCharacter] = useState([]);

  const url = 'https://cat-fact.herokuapp.com/facts';
  
  const apifetch = () => {
    fetch(url)
      .then((response) => response.json())
      //.then((data) => data.results) //console.log(data.results)
      .then((json) => {
        console.log(json);
        setCharacter(json)
      })
      .catch((error) => console.log(error))
  };

  useEffect(() => {
    apifetch(url)
  }, [])
  


  return (
    <div>
    <Header />
        <Cat character={character} />

    </div>
  );
}

export default App;
