import React from 'react'

const Cat = ({character = []}) => {
  return (
      <div className="container">
        <ul>
              {character.map(item =>
                  <li>{item.text}</li>
                )}  
      </ul>
    </div>
  )
}

export default Cat