import React from 'react'

const Header = () => {
  return (
    <nav className='navbar navbar-dark bg-dark'>
        <div className='container'>
              <p className='navbar-brand text-uppercase'>CAT FACTS</p>
        </div>
    </nav>
  )
}

export default Header